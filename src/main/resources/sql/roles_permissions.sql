INSERT INTO `roles_permissions`(`id`, `role_name`, `permission`) VALUES (1, 'admin', 'user:update');
INSERT INTO `roles_permissions`(`id`, `role_name`, `permission`) VALUES (2, 'admin', 'user:delete');
