package com.learn.session;

import com.learn.redis.JedisClient;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.SerializationUtils;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/11/23
 */
public class RedisSessionDao extends AbstractSessionDAO {

  @Autowired
  private JedisClient jedisClient;

  private final String SHIRO_SESSION_PREFIX = "shiro-session:";

  private byte[] getKey(String key) {
    return (SHIRO_SESSION_PREFIX + key).getBytes();
  }

  private void saveSession(Session session) {
    if (session != null && session.getId() != null) {
      byte[] key = getKey(session.getId().toString());
      byte[] value = SerializationUtils.serialize(session);
      jedisClient.set(key, value);
      jedisClient.expire(key, 1800);
    }
  }

  @Override
  protected Serializable doCreate(Session session) {
    Serializable sessionId = generateSessionId(session);
    this.assignSessionId(session, sessionId);
    saveSession(session);
    return sessionId;
  }

  @Override
  protected Session doReadSession(Serializable sessionId) {
    if (sessionId == null) {
      return null;
    }
    byte[] key = getKey(sessionId.toString());
    byte[] value = jedisClient.get(key);
    System.out.println("read session");
    Session session = (Session) SerializationUtils.deserialize(value);
    return session;
  }

  @Override
  public void update(Session session) throws UnknownSessionException {
    saveSession(session);
  }

  @Override
  public void delete(Session session) {
    if(session==null || session.getId()==null){
      return;
    }
    byte[] key = getKey(session.getId().toString());
    jedisClient.del(key);
  }

  @Override
  public Collection<Session> getActiveSessions() {
    Set<byte[]> keys = jedisClient.keys(SHIRO_SESSION_PREFIX);
    Set<Session> sessions = new HashSet<>();
    if(CollectionUtils.isEmpty(keys)){
      return sessions;
    }
    for(byte[] key : keys){
      Session session= (Session) SerializationUtils.deserialize(jedisClient.get(key));
      sessions.add(session);
    }
    return sessions;
  }
}
