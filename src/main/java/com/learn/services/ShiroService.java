package com.learn.services;

import java.util.Date;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.session.Session;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class ShiroService {

	public void testMethod(){
		System.out.println("testMethod, time: " + new Date());
	}
	
}
 