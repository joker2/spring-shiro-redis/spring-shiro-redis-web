/*
 * Copyright © 2017-2018 All Rights Reserved
 * 上海仰空网络科技有限公司 版权所有
 */

package com.learn.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
* @author wudhk
* @description 
* @date 2018/11/26
*/
@Data
@AllArgsConstructor
public class CustomException extends Exception {

  private String message;

}
