package com.learn.realms;

import com.learn.mapper.UserMapper;
import com.learn.mapper.UserRolesMapper;
import com.learn.pojo.User;
import java.util.HashSet;
import java.util.Set;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

public class CustomRealm extends AuthorizingRealm {

  @Autowired
  private UserMapper userMapper;
  @Autowired
  private UserRolesMapper userRolesMapper;
  @Override
  public String getName() {
    return "CustomRealm";
  }

  @Override
  public boolean supports(AuthenticationToken token) {
    // 仅支持 UsernamePasswordToken 类型的 Token
    return token instanceof UsernamePasswordToken;
  }

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token)
      throws AuthenticationException {
    String username = (String) token.getPrincipal();
    // 从数据库中根据用户名获取密码
    User user = userMapper.selByName(username);
    String password = user.getPassword();
    String salt = user .getPassword_salt();
    return new SimpleAuthenticationInfo(username, password, ByteSource.Util.bytes(salt), getName());
  }

  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    String username = (String) principals.getPrimaryPrincipal();
    // 从数据库中根据用户名获取角色权限
    System.out.println("从数据库取数据");
    Set<String> role = userRolesMapper.selByName(username);
    Set<String> permissions = new HashSet<>();
    permissions.add("user:update");
    permissions.add("user:delete");
    SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
    simpleAuthorizationInfo.setRoles(role);
    simpleAuthorizationInfo.setStringPermissions(permissions);
    return simpleAuthorizationInfo;
  }
}
