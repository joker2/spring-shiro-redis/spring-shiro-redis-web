package com.learn.realms;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.util.ByteSource;

public class SecondRealm extends AuthenticatingRealm {

  @Override
  public String getName() {
    return "SecondRealm";
  }

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(
      AuthenticationToken token) throws AuthenticationException {
    System.out.println("[SecondReaml] doGetAuthenticationInfo");

    //把 AuthenticationToken 转换为 UsernamePasswordToken
    UsernamePasswordToken upToken = (UsernamePasswordToken) token;

    //从 UsernamePasswordToken 中来获取 username
    String username = upToken.getUsername();

    //从数据库中根据用户名获取密码
    String password = "97e186e0592b4c755648b637f47c866b20dc9119";
    //盐值.
    String salt = "0ba2c03357cb7ba7";

    return new SimpleAuthenticationInfo(username, password, ByteSource.Util.bytes(salt), getName());
  }

  public static void main(String[] args) {
    String hashAlgorithmName = "SHA1";
    Object credentials = "1111";
    Object salt = ByteSource.Util.bytes("0ba2c03357cb7ba7");
    int hashIterations = 2;
    Object result = new SimpleHash(hashAlgorithmName, credentials, salt, hashIterations);
    System.out.println(result);
  }
}
