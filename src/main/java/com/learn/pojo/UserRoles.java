package com.learn.pojo;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/11/21
 */
public class UserRoles {
  private int id;
  private String username;
  private String role_name;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getRole_name() {
    return role_name;
  }

  public void setRole_name(String role_name) {
    this.role_name = role_name;
  }
}
