package com.learn.pojo;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/11/21
 */
public class User {

  private int id;
  private String username;
  private String password;
  private boolean rememberMe;
  private String password_salt;

  public boolean isRememberMe() {
    return rememberMe;
  }

  public void setRememberMe(boolean rememberMe) {
    this.rememberMe = rememberMe;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPassword_salt() {
    return password_salt;
  }

  public void setPassword_salt(String password_salt) {
    this.password_salt = password_salt;
  }
}
