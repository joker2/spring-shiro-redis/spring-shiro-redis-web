package com.learn.controller;

import com.learn.pojo.User;
import com.learn.services.ShiroService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/shiro")
public class ShiroController {

  @Autowired
  private ShiroService shiroService;

  @RequestMapping("/login")
  public String login(User user) {
    Subject currentUser = SecurityUtils.getSubject();
    if (!currentUser.isAuthenticated()) {
      UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername(), user.getPassword()  );
      try {
        token.setRememberMe(user.isRememberMe());
        currentUser.login(token);
        System.out.println("登录成功");
      }
      catch (AuthenticationException e) {
        System.out.println("登录失败: " + e.getMessage());
        return e.getMessage();
      }
    }
    return "redirect:/list.jsp";
  }

  @RequiresRoles({"admin"})
  @RequestMapping("/testShiroAnnotation")
  public String testShiroAnnotation(){
    shiroService.testMethod();
    return "redirect:/annotation.jsp";
  }

}
