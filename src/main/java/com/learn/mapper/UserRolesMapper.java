package com.learn.mapper;

import java.util.Set;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/11/21
 */
public interface UserRolesMapper {

  Set<String> selByName(String userNname);
}
