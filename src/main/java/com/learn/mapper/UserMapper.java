package com.learn.mapper;

import com.learn.pojo.User;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/11/21
 */
public interface UserMapper {

  User selByName(String userNname);
}
