package com.learn.redis;

import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/11/23
 */
@Repository
public class JedisClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(JedisClient.class);
  @Autowired
  private JedisPool jedisPool;

  private Jedis getResource() {
    return jedisPool.getResource();
  }

  public void set(byte[] key, byte[] value) {
    Jedis jedis = getResource();
    try {
      jedis.set(key, value);
    } catch (Exception e) {
      LOGGER.error("redis key:{} set value:{} occur exception", new String(key), new String(value));
      throw new RuntimeException("redis operation error:", e);
    } finally {
      jedis.close();
    }
  }

  public byte[] get(byte[] key) {
    Jedis jedis = getResource();
    byte[] value;
    try {
      value = jedis.get(key);
    } catch (Exception e) {
      LOGGER.error("redis key:{} get value occur exception", new String(key));
      throw new RuntimeException("redis operation error:", e);
    } finally {
      jedis.close();
    }
    return value;
  }

  public void expire(byte[] key, int i) {
    Jedis jedis = getResource();
    try {
      jedis.expire(key, i);
    } catch (Exception e) {
      LOGGER.error("redis key:{} expire value occur exception", new String(key));
      throw new RuntimeException("redis operation error:", e);
    } finally {
      jedis.close();
    }
  }

  public void del(byte[] key) {
    Jedis jedis = getResource();
    try {
      jedis.del(key);
    } catch (Exception e) {
      LOGGER.error("redis key:{} del value occur exception", new String(key));
      throw new RuntimeException("redis operation error:", e);
    } finally {
      jedis.close();
    }
  }

  public Set<byte[]> keys(String pattern) {
    Jedis jedis = getResource();
    Set keys = null;
    try {
      keys = jedis.keys((pattern + "*").getBytes());
    } catch (Exception e) {
      LOGGER.error("redis get keys in pattern:{} occur exception", pattern);
      throw new RuntimeException("redis operation error:", e);
    } finally {
      jedis.close();
    }
    return keys;
  }

}