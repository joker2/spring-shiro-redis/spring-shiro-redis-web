package com.learn.cache;

import com.learn.redis.JedisClient;
import java.util.Collection;
import java.util.Set;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.SerializationUtils;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/11/22
 */
@Component
public class RedisCache<K,V> implements Cache<K,V> {

  @Autowired
  private JedisClient jedisClient;

  private final String CACHE_PREFIX="shiro-cache";

  private byte[] getKey(K k){
    if(k instanceof String){
      return (CACHE_PREFIX+k).getBytes();
    }
    return SerializationUtils.serialize(k);
  }
  @Override
  public V get(K k) throws CacheException {
    System.out.println("从redis取数据");
    byte[] value = jedisClient.get(getKey(k));
    if(value!=null) {
      return (V) SerializationUtils.deserialize(value);
    }
    return null;
  }

  @Override
  public V put(K k, V v) throws CacheException {
    byte[] key = getKey(k);
    byte[] value =SerializationUtils.serialize(v);
    jedisClient.set(key,value);
    jedisClient.expire(key,1800);
    return v;
  }

  @Override
  public V remove(K k) throws CacheException {
    byte[] key = getKey(k);
    byte[] value = jedisClient.get(key);
    jedisClient.del(key);
    if(value!=null){
      return (V)SerializationUtils.deserialize(value);
    }
    return null;
  }

  @Override
  public void clear() throws CacheException {

  }

  @Override
  public int size() {
    return 0;
  }

  @Override
  public Set<K> keys() {
    return null;
  }

  @Override
  public Collection<V> values() {
    return null;
  }
}
