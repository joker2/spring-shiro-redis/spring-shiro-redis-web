package com.learn.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/11/22
 */
public class RedisCacheManager implements CacheManager {

  @Autowired
  private RedisCache redisCache;

  @Override
  public <K, V> Cache<K, V> getCache(String s) throws CacheException {
    return redisCache;
  }
}
